
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class TheScore {

	public static void main(String[] args) throws InterruptedException {

		// Set the Desired Capabilities
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("deviceName", "Earl's s8");
		caps.setCapability("udid", "98897a433935385a4a"); // Give Device ID of
															// your mobile phone
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", "6.0");
		caps.setCapability("appPackage", "com.fivemobile.thescore");
		caps.setCapability("appActivity", "com.fivemobile.thescore.SplashActivity");
		caps.setCapability("noReset", "true");

		// Instantiate Appium Driver
		try {
			AppiumDriver<MobileElement> driver = new AndroidDriver<MobileElement>(
					new URL("http://192.168.0.106:4723/wd/hub"), caps);
			Thread.sleep(10000);

			// Leagues Tab
			try {

				if (driver.findElement(By.xpath("//*[(@resource-id='com.fivemobile.thescore:id/action_leagues')]"))
						.isDisplayed() == true) {
					System.out.println("Leagues tab on bottom right side is displayed");
					Thread.sleep(2000);

					driver.findElement(By.xpath("//*[(@resource-id='com.fivemobile.thescore:id/action_leagues')]"))
							.click();
					System.out.println("Leagues tab on bottom right side is clicked");
					Thread.sleep(2000);

				}

			} catch (Exception E) {
			}
			Thread.sleep(5000);

			// NBA Basketball
			try {
				if (driver
						.findElement(By
								.xpath("//*[(@text='NBA Basketball') and (@resource-id='com.fivemobile.thescore:id/league_name_text')]"))
						.isDisplayed() == true) {
					System.out.println("NBA Basketball option is displayed");
					Thread.sleep(2000);

					driver.findElement(By
							.xpath("//*[(@text='NBA Basketball') and (@resource-id='com.fivemobile.thescore:id/league_name_text')]"))
							.click();
					System.out.println("NBA Basketball option is clicked");

				}
			} catch (Exception E) {
				System.out.println(E);

			}
			Thread.sleep(5000);

			// Leaders Tab
			try {
				if (driver.findElement(By.xpath("//*[(@text='LEADERS') and (@class='android.widget.TextView')]"))
						.isDisplayed() == true) {
					System.out.println("Leaders tab is displayed");
					Thread.sleep(2000);

					driver.findElement(By.xpath("//*[(@text='LEADERS') and (@class='android.widget.TextView')]"))
							.click();
					System.out.println("Leaders tab is clicked");
					Thread.sleep(2000);

				}
			} catch (Exception E) {
				System.out.println(E);
			}
			Thread.sleep(5000);
			try {

				String PlayerName = driver
						.findElement(By.xpath("//*[(@resource-id='com.fivemobile.thescore:id/txt_name')]")).getText();
				Thread.sleep(2000);
				System.out.println("Player " + PlayerName
						+ " from Leader's tab should be displayed in the next Player's info page");

				driver.findElement(By.xpath("//*[(@resource-id='com.fivemobile.thescore:id/txt_name')]")).click();

				Thread.sleep(5000);

				String PlayerNamePage2 = driver
						.findElement(By.xpath("//*[(@resource-id='com.fivemobile.thescore:id/player_header')]"))
						.getText();
				Thread.sleep(2000);

				if (PlayerName.contains(PlayerNamePage2)) {
					System.out.println("Player name " + PlayerName
							+ " is displayed and validated on Player info page from the Leader's tab");
				}
				Thread.sleep(2000);

			} catch (Exception E) {
				System.out.println(E);
			}

		} catch (MalformedURLException e) {
			System.out.println(e.getMessage());
		}
		Thread.sleep(10000);

	}

}
